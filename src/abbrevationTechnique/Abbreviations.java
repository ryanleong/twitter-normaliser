//======================================================
// Knowledge Technologies Project 1 (2013)
// Twitter Normaliser
// 395463 (wsleong) Ryan Leong
//======================================================

package abbrevationTechnique;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import normaliser.WordToken;

public class Abbreviations {

	public ArrayList<WordToken> checkAbbreviationDictionary(ArrayList<WordToken> tokens) {

		BufferedReader br = null;

		try {

			String currentLine;

			// new bufferedReader
			br = new BufferedReader(new FileReader(
					"dictionary/abbreviations.txt"));

			// read line by line
			while ((currentLine = br.readLine()) != null) {
				String parts[] = currentLine.split(",");

				for (int i = 0; i < tokens.size(); i++) {

					// if token is in dictionary
					if (tokens.get(i).tokenType != "IV"
							&& tokens.get(i).token.equals(parts[0])) {
						tokens.get(i).setTokenType("IV");
						tokens.get(i).setNormalizedString(parts[1]);
					}
				}

			}

		} catch (IOException e) {
			// print io exception
			e.printStackTrace();
		} finally {

			// close bufferedReader
			try {
				if (br != null) {
					br.close();
				}
			} catch (IOException ex) {
				ex.printStackTrace();
			}

		}
		
		return tokens;

	}
}
