//======================================================
// Knowledge Technologies Project 1 (2013)
// Twitter Normaliser
// 395463 (wsleong) Ryan Leong
//======================================================

package normaliser;

public class WordToken {

	public String token;
	public String tokenType;
	public String normalizedString;
	
	public WordToken() {
		token = null;
		tokenType = null;
		normalizedString = null;
	}
	
	public WordToken(String x, String y, String z) {
		token = x;
		tokenType = y;
		normalizedString = z;
	}
	
	
	// set methods
	public void setToken(String x) {
		token = x;
	}
	
	public void setTokenType(String x) {
		tokenType = x;
	}
	
	public void setNormalizedString(String x) {
		normalizedString = x;
	}
	
	// get methods
	public String getToken() {
		return token;
	}
	
	public String getTokenType() {
		return tokenType;
	}
	
	public String getNormalizedString() {
		return normalizedString;
	}
}
