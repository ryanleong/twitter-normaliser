//======================================================
// Knowledge Technologies Project 1 (2013)
// Twitter Normaliser
// 395463 (wsleong) Ryan Leong
//======================================================

package normaliser;

public class DictionaryWord {

	public String word;
	public int distance;
	public int utility;
	
	public DictionaryWord() {
		word = null;
		distance = -1;
		utility = 0;
	}
	
	public DictionaryWord(String x, int y, int z) {
		word = x;
		distance = y;
		utility = z;
	}
	
}
