//======================================================
// Knowledge Technologies Project 1 (2013)
// Twitter Normaliser
// 395463 (wsleong) Ryan Leong
//======================================================

// To compile with java 1.6 on CIS servers: /usr/java1.6/bin/java or /usr/java1.6/bin/javac

package normaliser;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

import abbrevationTechnique.Abbreviations;
import algorithms.NeedlemanWunsh;
import algorithms.Ngram;
import algorithms.Soundex;

import frequencyTechnique.Frequency;

public class Main {

	// ////////////////////////////////////////////////////////////////////////
	// Settings
	// ////////////////////////////////////////////////////////////////////////
	// technique
	// Sets which technique to use for normalisation
	// 1 - Ngram
	// 2 - Ngram & Needleman-Wunsch
	// 3 - Ngram, Needleman-Wunsch & Soundex
	// 4 - Ngram, Needleman-Wunsch & Frequency Search
	private static int technique = 2;

	// ////////////////////////////////////////////////////////////////////////
	// evaluationOuput
	// If true, the first of the list of possibile normalisation words is
	// chosen as the evaluation script only allows one word
	// If false, the list of possible normalisation words is combined into
	// a string before output. This is so that a human evaluator can check
	// through the list.
	private static boolean evaluationOutput = true;

	// ////////////////////////////////////////////////////////////////////////
	// useAbbreviationDictionary
	// If true, an abbreviation dictionary is also checkd for IV words
	private static boolean useAbbreviationDictionary = false;

	// Global Variables
	private static String dictFileName = "dictionary/words.txt";
	private static String inputFile = "corpus.test";
	public static ArrayList<WordToken> tokens = new ArrayList<WordToken>();

	public static void main(String[] args) {

		// read in arguments
		if (args.length != 0) {
			try {
				
				// if -h flag is used
				if (args[0].equals("-h")) {
					
					System.out.println("usage: \n/usr/java1.6/bin/java -Xmx128m -cp bin:. " + 
							"normaliser.Main [-m] [-a] [-d file] [-t algorithm]\n" +
							"-a\t\t: To enable use of abbreviation dictionary\n" +
							"-d file\t\t: Location of dictionary file\n" +
							"-f file\t\t: Location of input file\n" +
							"-h\t\t: This message\n" +
							"-m\t\t: To enable output for human comparison\n" +
							"-t algorithm\t: An integer that represents the use of 1 of the 4 algorithms" +
							"\n\t\t\t 1 - Ngram\n\t\t\t 2 - Ngram & Needleman-Wunsch\n" +
							"\t\t\t 3 - Ngram, Needleman-Wunsch & Soundex" +
							"\n\t\t\t 4 - Ngram, Needleman-Wunsch & Frequency Search (untested)");

					System.exit(0);
				}

				for (int i = 0; i < args.length; i++) {
					
					// if -t flag is used
					if (args[i].equals("-t")) {
						technique = Integer.parseInt(args[i + 1]);

						if (technique < 1 || technique > 4) {
							System.out
									.println("Choose between algorithms 1, 2, 3 or 4.");
							System.exit(0);
						}
					} 
					// if -d flag is used
					else if (args[i].equals("-d")) {
						dictFileName = args[i + 1];

						File f = new File(dictFileName);

						if (!f.exists()) {
							System.out.println("Dictionary not found.");
							System.exit(0);
						}
					} 
					// if -m flag is used
					else if (args[i].equals("-m")) {
						
						evaluationOutput = false;
					} 
					// if -a flag is used
					else if (args[i].equals("-a")) {
						
						useAbbreviationDictionary = true;
					}
					// if -f flag is used
					else if (args[i].equals("-f")) {
						
						inputFile = args[i + 1];
						
						File f = new File(inputFile);

						if (!f.exists()) {
							System.out.println("Input file not found.");
							System.exit(0);
						}
					}
				}

			} catch (Exception e) {
				System.out.println("Invalid arguments\nRunning with default settings");
			}
		}


		BufferedReader br = null;
		BufferedWriter bw = null;

		int tokenCount = 0, noOfTokens = 0;
		boolean newTweet = true;

		// location of output file
		File file = new File("corpus.attempt");

		// get start time to time program
		long startTime = System.currentTimeMillis();
		System.out.println("Process started...\nAverage run time for the default settings is ~60 secs.");

		try {

			String currentLine;

			// new bufferedReader
			br = new BufferedReader(new FileReader(inputFile));

			// create new output file
			file.createNewFile();
			FileWriter fw = new FileWriter(file.getAbsoluteFile());
			bw = new BufferedWriter(fw);

			// read line by line
			while ((currentLine = br.readLine()) != null) {

				if (newTweet) {
					// read word count
					noOfTokens = Integer.parseInt(currentLine);
					newTweet = false;
					bw.write(currentLine + "\n");

				} else {

					// add tokens to ArrayList
					if (tokenCount < noOfTokens) {
						tokens.add(new WordToken(currentLine, null, null));
						tokenCount++;

						// to-do: check if stated no. of tokens is incorrect

						if (tokenCount == noOfTokens) {
							// get all tokens which are in dictionary
							processTokens();

							// write to file
							for (int i = 0; i < tokens.size(); i++) {
								bw.write(tokens.get(i).token + "\t"
										+ tokens.get(i).tokenType + "\t"
										+ tokens.get(i).normalizedString + "\n");
							}

							// reset for new tweet
							noOfTokens = 0;
							tokenCount = 0;
							newTweet = true;
							tokens = new ArrayList<WordToken>();
						}
					}

				}

			}

		} catch (IOException e) {
			// print io exception
			e.printStackTrace();
		} finally {

			// close bufferedReader
			try {
				if (br != null) {
					br.close();
				}

				if (bw != null) {
					bw.close();
				}
			} catch (IOException ex) {
				ex.printStackTrace();
			}

		}

		System.out.println("Processed completed\nTime Taken: "
				+ (System.currentTimeMillis() - startTime) / 1000);
	}

	// /////////////////////////////////////////////////////////////////
	// Name: processTokens()
	// Parameters:
	// Return:
	// Description: Identify the IV tokens, Evaluate the OOV tokens
	// /////////////////////////////////////////////////////////////////
	private static void processTokens() {
		// check if word is in dictionary
		checkDictionary();

		// If enabled, use abbreviation dictionary
		if (useAbbreviationDictionary) {
			tokens = new Abbreviations().checkAbbreviationDictionary(tokens);
		}

		for (int i = 0; i < tokens.size(); i++) {

			if (tokens.get(i).tokenType == "IV")
				continue;

			// check if token is username, hashtag, nonword, URL respectively
			if (tokens.get(i).token.matches("^@[a-zA-Z0-9_-]+$")
					|| tokens.get(i).token.matches("^#[a-zA-Z0-9_-]+$")
					|| tokens.get(i).token.matches("^[^a-zA-Z]+$")
					|| tokens.get(i).token
							.matches("^(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]")) {

				tokens.get(i).setTokenType("NO");
				tokens.get(i).setNormalizedString(tokens.get(i).token);
			}
			// if token is not in dictionary and a word
			else {
				// set tokens to OOV
				tokens.get(i).setTokenType("OOV");

				// check if token should not be normalised
				// Assumes that all words with ' are short forms
				// e.g. can't, i'm
				if (tokens.get(i).token.matches("^.*[\\-._!@#$%^&*/()']+.*$")) {
					tokens.get(i).normalizedString = tokens.get(i).token;
					continue;
				}

				// check for 3 or more consecutive chars and reduce them to 1
				String temp = tokens.get(i).token;
				tokens.get(i).token = tokens.get(i).token.replaceAll("(.)\\1+",
						"$1");

				// call N-gram normaliser
				ArrayList<DictionaryWord> ngrams = getApproxWordNgram(tokens
						.get(i).token);

				ArrayList<DictionaryWord> needlemanwunsh = null;

				// If more than 1 value returned from N-gram normaliser
				// check which technique is being used
				if ((technique == 2 || technique == 3 || technique == 4)
						&& ngrams.size() > 1) {

					// call Needleman-Wunsh normaliser
					needlemanwunsh = getApproxWordNeedlemanWunsh(tokens.get(i).token);

					// Arraylist of best normalisation words
					ArrayList<DictionaryWord> topWords = new ArrayList<DictionaryWord>();

					// if using technique 2
					if (technique == 2 || technique == 3) {

						// set utility value for both N-gram and Needleman-Wunsh
						// results
						// (p+2) * (k+2)
						for (int j = 0; j < ngrams.size(); j++) {
							for (int k = 0; k < needlemanwunsh.size(); k++) {

								// set utility value if 2 words match
								if (ngrams.get(j).word.equals(needlemanwunsh
										.get(k).word)) {

									ngrams.get(j).utility = (ngrams.get(j).distance + needlemanwunsh
											.get(k).distance) * 2;
									needlemanwunsh.get(k).utility = ngrams
											.get(j).utility;
								}
							}
						}

						// combine both arraylist
						ngrams.addAll(needlemanwunsh);

						// remove duplicates
						ArrayList<DictionaryWord> tempList = removeDuplicates(ngrams);

						int prevUtil = tempList.get(0).utility;

						// get all words with lowest utility value
						for (int j = 0; j < tempList.size(); j++) {

							// store closest few in arraylist
							if (tempList.get(j).utility < prevUtil) {
								topWords.clear();
								topWords.add(tempList.get(j));

								prevUtil = tempList.get(j).utility;
							} else if (tempList.get(j).utility == prevUtil) {
								topWords.add(tempList.get(j));
							}
						}

						// call soundex to compare if still undecided
						// check which technique is being used
						if (technique == 3 && topWords.size() > 1) {
							topWords = getApproxWordSoundEx(
									tokens.get(i).token, topWords);

						}
					}

					// This extra function is not fully tested yet.
					// This technique uses the frequency of words in tungev.txt
					// to
					// determine which word should appear next
					if (technique == 4) {

						// generate frequency dictionary if dictionary not found
						new Frequency().generateFrequency();

						// combine both arraylist
						ngrams.addAll(needlemanwunsh);

						// remove duplicates
						topWords = removeDuplicates(ngrams);

						// store normalised string
						if (topWords.size() > 1) {

							String tempWord = getApproxWordFrequency(topWords);

							if (!tempWord.equals("")) {
								tokens.get(i).setNormalizedString(tempWord);
							}
						}
					}

					// save normalised word
					saveNormalisedWord(i, topWords);

				} else {
					// if Ngram returns only 1 result. set word as the
					// normalised word

					saveNormalisedWord(i, ngrams);
				}

				// set token back if it was reduced early due to multiple
				// consecutive chars
				tokens.get(i).token = temp;
			}
		}
	}

	// /////////////////////////////////////////////////////////////////
	// Name: saveNormalisedWord()
	// Parameters: int index, ArrayList<DictionaryWord> topWords
	// Return:
	// Description: save the normalised word
	// /////////////////////////////////////////////////////////////////
	private static void saveNormalisedWord(int index,
			ArrayList<DictionaryWord> topWords) {

		// if true, output is formatted for evaluation script
		if (evaluationOutput) {
			// pick the first word from string as the output
			tokens.get(index).setNormalizedString(topWords.get(0).word);
		} else {

			// output as string of all possibilities
			String tok = "";

			for (int j = 0; j < topWords.size(); j++) {

				tok += topWords.get(j).word + ", ";
			}

			tokens.get(index).setNormalizedString(tok);
		}
	}

	// /////////////////////////////////////////////////////////////////
	// Name: removeDuplicates()
	// Parameters: ArrayList<DictionaryWord> dw
	// Return: ArrayList<DictionaryWord>
	// Description: Takes an arraylist of DictionaryWord and returns it
	// without duplicates
	// /////////////////////////////////////////////////////////////////
	private static ArrayList<DictionaryWord> removeDuplicates(
			ArrayList<DictionaryWord> dw) {

		// remove duplicates
		for (int i = 0; i < dw.size(); i++) {
			for (int j = i + 1; j < dw.size(); j++) {
				if (dw.get(i).word.equals(dw.get(j).word)) {
					dw.remove(j);
				}
			}
		}

		// sets utility value for words that do not appear in both list
		for (int i = 0; i < dw.size(); i++) {
			if (dw.get(i).utility == 0) {
				dw.get(i).utility = dw.get(i).utility + 20;
			}
		}

		return dw;
	}

	// /////////////////////////////////////////////////////////////////
	// Name: additionalSoundexProcessing()
	// Parameters: String word, ArrayList<DictionaryWord> currentList
	// Return: ArrayList<DictionaryWord>
	// Description:
	// /////////////////////////////////////////////////////////////////
	private static ArrayList<DictionaryWord> getApproxWordSoundEx(String word,
			ArrayList<DictionaryWord> currentList) {

		ArrayList<DictionaryWord> tempList = new ArrayList<DictionaryWord>();
		ArrayList<DictionaryWord> topWords = new ArrayList<DictionaryWord>();

		// call soundex normaliser
		ArrayList<DictionaryWord> soundex = additionalSoundexProcessing(word);

		// set utility value for both N-gram and Needleman-Wunsh results
		// (p+2) * (k+2)
		for (int j = 0; j < currentList.size(); j++) {
			for (int k = 0; k < soundex.size(); k++) {

				// set utility value if 2 words match
				if (currentList.get(j).word.equals(soundex.get(k).word)) {

					currentList.get(j).utility = (currentList.get(j).distance + soundex
							.get(k).distance) * 2;
					soundex.get(k).utility = currentList.get(j).utility;
				}
			}
		}

		// combine both arraylist
		currentList.addAll(soundex);

		// remove duplicates
		tempList = removeDuplicates(currentList);

		int prevUtil = tempList.get(0).utility;

		// get all words with lowest utility value
		for (int j = 0; j < tempList.size(); j++) {

			// store closest few in arraylist
			if (tempList.get(j).utility < prevUtil) {
				topWords.clear();
				topWords.add(tempList.get(j));

				prevUtil = tempList.get(j).utility;
			} else if (tempList.get(j).utility == prevUtil) {
				topWords.add(tempList.get(j));
			}
		}

		return topWords;
	}

	// /////////////////////////////////////////////////////////////////
	// Name: processTokens()
	// Parameters:
	// Return:
	// Description: Find all tokens which are in the dictionary
	// /////////////////////////////////////////////////////////////////
	private static void checkDictionary() {

		String s = null;
		String searchTokens = "";
		BufferedReader stdInput = null;
		Process p = null;

		// place tokens into string for egrep
		for (int i = 0; i < tokens.size(); i++) {

			// check that token is not a symbol
			if (tokens.get(i).token.matches("^[a-zA-Z]+$")) {
				searchTokens += tokens.get(i).token + "|";
			}

		}

		String[] cmd = { "grep", "-E", "^(" + searchTokens + ")$", dictFileName };

		// check if token
		try {

			// check if in word dictionary
			p = Runtime.getRuntime().exec(cmd);

			stdInput = new BufferedReader(new InputStreamReader(
					p.getInputStream()));

			while ((s = stdInput.readLine()) != null) {

				// place IV type into token object for each token
				for (int i = 0; i < tokens.size(); i++) {

					// if token is in dictionary
					if (tokens.get(i).token.equals(s)) {
						tokens.get(i).setTokenType("IV");
						tokens.get(i).setNormalizedString(tokens.get(i).token);
					}
				}
			}

			// prepare tokens for regex in surname dictionary
			searchTokens = "";

			for (int i = 0; i < tokens.size(); i++) {
				if (!tokens.get(i).token.equals("IV")) {
					searchTokens += tokens.get(i).token + "|";
				}

			}

			String[] cmd2 = { "grep", "-E", "^(" + searchTokens + ")$",
					"dictionary/surname.txt" };

			// check if in name dictionary
			p = Runtime.getRuntime().exec(cmd2);

			stdInput = new BufferedReader(new InputStreamReader(
					p.getInputStream()));

			while ((s = stdInput.readLine()) != null) {

				// place IV type into token object for each token
				for (int i = 0; i < tokens.size(); i++) {

					// if token is in dictionary
					if (tokens.get(i).token.equals(s)) {
						tokens.get(i).setTokenType("IV");
						tokens.get(i).setNormalizedString(tokens.get(i).token);
					}
				}
			}

		} catch (IOException e) {
			e.printStackTrace();
		} finally {

			// close bufferedReader
			try {
				if (stdInput != null) {
					stdInput.close();
				}
			} catch (IOException ex) {
				ex.printStackTrace();
			}

		}

	}

	private static ArrayList<DictionaryWord> getApproxWordNeedlemanWunsh(
			String word) {

		BufferedReader br = null;

		ArrayList<DictionaryWord> possibleWords = new ArrayList<DictionaryWord>();

		try {

			String currentLine;

			// read from file
			br = new BufferedReader(new FileReader(dictFileName));
			int prevDist = 1000, dist = 0;

			// read line by line
			while ((currentLine = br.readLine()) != null) {

				// only consider words with +- 2 letters
				// this is to reduce CPU cycles
				if (word.length() + 2 >= currentLine.length()
						&& word.length() - 2 <= currentLine.length()) {

					dist = new NeedlemanWunsh().getDistance(word, currentLine);

					// store closest few in arraylist
					if (dist < prevDist) {
						possibleWords.clear();
						possibleWords.add(new DictionaryWord(currentLine, dist,
								0));

						prevDist = dist;
					} else if (dist == prevDist) {
						possibleWords.add(new DictionaryWord(currentLine, dist,
								0));
					}

				}
			}

		} catch (IOException e) {
			// print io exception
			e.printStackTrace();
		} finally {

			// close bufferedReader
			try {
				if (br != null) {
					br.close();
				}
			} catch (IOException ex) {
				ex.printStackTrace();
			}

		}

		return possibleWords;
	}

	// /////////////////////////////////////////////////////////////////
	// Name: getApproximateWord()
	// Parameters: String word
	// Return: String
	// Description: Finds the closest words and returns it. This ommits
	// 1 char dictionary entries
	// /////////////////////////////////////////////////////////////////
	private static ArrayList<DictionaryWord> getApproxWordNgram(String word) {

		BufferedReader br = null;

		ArrayList<DictionaryWord> possibleWords = new ArrayList<DictionaryWord>();

		try {

			String currentDictionaryWord;

			// new bufferedReader
			br = new BufferedReader(new FileReader(dictFileName));
			int prevDist = 1000, dist = 0;

			// read line by line
			while ((currentDictionaryWord = br.readLine()) != null) {

				// only consider words with +- 2 letters
				// this is to reduce CPU cycles
				if (currentDictionaryWord.length() != 1
						&& word.length() + 2 >= currentDictionaryWord.length()
						&& word.length() - 2 <= currentDictionaryWord.length()) {

					dist = new Ngram().getDistance(word, currentDictionaryWord);

					// store closest few in arraylist
					if (dist < prevDist) {
						possibleWords.clear();
						possibleWords.add(new DictionaryWord(
								currentDictionaryWord, dist, 0));

						prevDist = dist;
					} else if (dist == prevDist) {
						possibleWords.add(new DictionaryWord(
								currentDictionaryWord, dist, 0));
					}

				}
			}

		} catch (IOException e) {
			// print io exception
			e.printStackTrace();
		} finally {

			// close bufferedReader
			try {
				if (br != null) {
					br.close();
				}
			} catch (IOException ex) {
				ex.printStackTrace();
			}

		}

		return possibleWords;
	}

	// /////////////////////////////////////////////////////////////////
	// Name: getApproximateWordSoundEx()
	// Parameters: ArrayList<Dictionary Word>, String word
	// Return: ArrayList<Dictionary Word>
	// Description: Takes list of possible words and uses a Soundex to
	// narrow down the choices.
	// /////////////////////////////////////////////////////////////////
	private static ArrayList<DictionaryWord> additionalSoundexProcessing(
			String word) {
		BufferedReader br = null;

		ArrayList<DictionaryWord> possibleWords = new ArrayList<DictionaryWord>();

		try {

			String currentDictionaryWord;

			// new bufferedReader
			br = new BufferedReader(new FileReader(dictFileName));
			int prevDist = 1000, dist = 0;

			// read line by line
			while ((currentDictionaryWord = br.readLine()) != null) {

				// check for words that are +-2 in length from string being
				// checked
				// and that it is not a 1 char string
				if (currentDictionaryWord.length() != 1
						&& word.length() + 2 >= currentDictionaryWord.length()
						&& word.length() - 2 <= currentDictionaryWord.length()) {

					dist = new Soundex().getDistance(word,
							currentDictionaryWord);

					// store closest few in arraylist
					if (dist < prevDist) {
						possibleWords.clear();
						possibleWords.add(new DictionaryWord(
								currentDictionaryWord, dist, 0));

						prevDist = dist;
					} else if (dist == prevDist) {
						possibleWords.add(new DictionaryWord(
								currentDictionaryWord, dist, 0));
					}
				}

			}

		} catch (IOException e) {
			// print io exception
			e.printStackTrace();
		} finally {

			// close bufferedReader
			try {
				if (br != null) {
					br.close();
				}
			} catch (IOException ex) {
				ex.printStackTrace();
			}

		}

		return possibleWords;
	}

	private static String getApproxWordFrequency(
			ArrayList<DictionaryWord> candidates) {

		String s = null;
		String searchTokens = "";
		BufferedReader stdInput = null;
		Process p = null;

		int topFrequency = 0;
		String bestWord = "";

		// place tokens into string for egrep
		for (int i = 0; i < candidates.size(); i++) {
			searchTokens += candidates.get(i).word.toLowerCase() + "|";
		}

		String[] cmd = { "grep", "-E", "^(" + searchTokens + "),[0-9]+$",
				"dictionary/wordfrequency.txt" };

		// check if token
		try {

			// check if in word dictionary
			p = Runtime.getRuntime().exec(cmd);

			stdInput = new BufferedReader(new InputStreamReader(
					p.getInputStream()));

			while ((s = stdInput.readLine()) != null) {
				String[] partStrings = s.split(",");

				// find the word with the highest frequency
				if (Integer.parseInt(partStrings[1]) > topFrequency) {
					topFrequency = Integer.parseInt(partStrings[1]);
					bestWord = partStrings[0];
				}

			}

		} catch (IOException e) {
			e.printStackTrace();
		} finally {

			// close bufferedReader
			try {
				if (stdInput != null) {
					stdInput.close();
				}
			} catch (IOException ex) {
				ex.printStackTrace();
			}

		}

		return bestWord;
	}

}
