//======================================================
// Knowledge Technologies Project 1 (2013)
// Twitter Normaliser
// 395463 (wsleong) Ryan Leong
//======================================================

package algorithms;

public class Ngram {

	// /////////////////////////////////////////////////////////////////
	// Name: getDistance()
	// Parameters: String x (word), String y (dictionary word)
	// Return: int
	// Description: takes 2 strings and return the distance value between them
	// using N-gram
	// algorithm
	// /////////////////////////////////////////////////////////////////
	public int getDistance(String x, String y) {

		String[] xNGram = null;
		String[] yNGram = null;

		x = x.toLowerCase();
		y = y.toLowerCase();

		int sameCount = 0;

		xNGram = new String[x.length() - 1];
		yNGram = new String[y.length() - 1];

		// form N-grams for word
		for (int i = 0; i < x.length(); i++) {
			if (i < x.length() - 1) {
				xNGram[i] = "" + x.charAt(i) + x.charAt(i + 1);
			}
		}

		// form N-grams for dictionary word
		for (int i = 0; i < y.length(); i++) {
			if (i < y.length() - 1) {
				yNGram[i] = "" + y.charAt(i) + y.charAt(i + 1);
			}
		}

		// find number of same N-grams
		for (int i = 0; i < xNGram.length; i++) {
			for (int j = 0; j < yNGram.length; j++) {

				if (xNGram[i].equals(yNGram[j])) {
					sameCount++;
					// yNGram[j] = "";
					// break;
				}
			}
		}

		return (x.length() + y.length()) - (2 * sameCount);
	}
}
