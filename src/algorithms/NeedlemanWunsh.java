//======================================================
// Knowledge Technologies Project 1 (2013)
// Twitter Normaliser
// 395463 (wsleong) Ryan Leong
//======================================================

package algorithms;

public class NeedlemanWunsh {

	// /////////////////////////////////////////////////////////////////
	// Name: getDistance()
	// Parameters: String x, String y
	// Return: int
	// Description: takes 2 strings and return the distance value
	//				between them using Needleman-Wunsh
	// /////////////////////////////////////////////////////////////////
	public int getDistance(String x, String y) {
		
	    int	l1, l2;
	    x = " " + x.toLowerCase();
	    y = " " + y.toLowerCase();
	    
	    
	    l1 = x.length();
	    l2 = y.length();
	    int[][] f = new int[l1][l2];
	    
	    // initialise first row of each column
	    for (int i = 0; i < l1; i++) { f[i][0] = i; }
	    for (int j = 0; j < l2; j++) { f[0][j] = j; }
	    
	    // create 2D array of distances
	    for(int i = 1; i < l1; i++) {
		    for (int j = 1; j < l2; j++) {
				f[i][j] = Math.min(f[i-1][j] + 1,	// insertion 
						Math.min(f[i][j-1] + 1,		// deletion
						f[i-1][j-1] + equal(x.charAt(i), y.charAt(j))));	// match/mismatch
			}
		}
	    
		return f[l1-1][l2-1];
	}
	
	// /////////////////////////////////////////////////////////////////
	// Name: equal()
	// Parameters: char x, char y
	// Return: int
	// Description: takes 2 char. returns 0 if they are equal, 1 if different
	// /////////////////////////////////////////////////////////////////
	private int equal(char x, char y) {
		
		if (x == y) {
			return 0;
		} else {
			return 1;
		}
	}
}
