//======================================================
// Knowledge Technologies Project 1 (2013)
// Twitter Normaliser
// 395463 (wsleong) Ryan Leong
//======================================================

package algorithms;

public class Soundex {
	// /////////////////////////////////////////////////////////////////
	// Name: getDistance()
	// Parameters: String x, String y
	// Return: int
	// Description: takes 2 strings and return the distance value
	//				between them using Soundex
	// /////////////////////////////////////////////////////////////////
	public int getDistance(String x, String y) {
		
		String xCode = "";
		String yCode = "";
		
		 x = x.toLowerCase();
		 y = y.toLowerCase();
		
		// translate to digit codes without first char
		for (int i = 1; i < x.length(); i++) {
			xCode += getCode(x, i);
		}
		
		for (int i = 1; i < y.length(); i++) {
			yCode += getCode(y, i);
		}
		
		// remove consecutive same chars
		xCode = xCode.replaceAll("(.)\\1", "$1");
		yCode = yCode.replaceAll("(.)\\1", "$1");
		
		// remove all 0s
		xCode = xCode.replaceAll("0", "");
		yCode = yCode.replaceAll("0", "");
		
		// add first char of string back to code
		xCode = x.charAt(0) + xCode;
		yCode = y.charAt(0) + yCode;
		
		// use N-gram to determine distance before returning
		return new Ngram().getDistance(xCode, yCode);
	}
	
	private String getCode(String word, int position) {
		
		// check for bfpv
		if (word.charAt(position) == 'b' ||
				word.charAt(position) == 'f' ||
				word.charAt(position) == 'p' ||
				word.charAt(position) == 'v') {
			return "1";
		}
		// check for cgjkqsxz
		else if (word.charAt(position) == 'c' ||
				word.charAt(position) == 'g' ||
				word.charAt(position) == 'j' ||
				word.charAt(position) == 'k' ||
				word.charAt(position) == 'q' ||
				word.charAt(position) == 's' ||
				word.charAt(position) == 'x' ||
				word.charAt(position) == 'z') {
			return "2";
		}
		else if (word.charAt(position) == 'd' ||
				word.charAt(position) == 't') {
			return "3";
		}
		else if (word.charAt(position) == 'l') {
			return "4";
		}
		else if (word.charAt(position) == 'm' ||
				word.charAt(position) == 'n') {
			return "5";
		}
		else if (word.charAt(position) == 'r') {
			return "6";
		}
		else {
			return "0";
		}
	}
}
