//======================================================
// Knowledge Technologies Project 1 (2013)
// Twitter Normaliser
// 395463 (wsleong) Ryan Leong
//======================================================

package frequencyTechnique;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class Frequency {

	public ArrayList<FrequencyWord> frequencyDictionary = new ArrayList<FrequencyWord>();

	public void generateFrequency() {

		File f = new File("dictionary/wordfrequency.txt");

		// check if dictionary exist
		if (!f.exists()) {
			
			System.out.println("Generating frequency dictionary as it is not found.");

			BufferedReader br = null;
			BufferedWriter bw = null;

			// location of output file
			File file = new File("dictionary/wordfrequency.txt");

			try {

				String currentLine;

				// new bufferedReader
				br = new BufferedReader(new FileReader(
						"dictionary/turgenev.txt"));

				// read line by line
				while ((currentLine = br.readLine()) != null) {
					String[] words = currentLine.split("\\s+");

					for (String s : words) {
						String currentWordString = s
								.replaceAll("[^A-Za-z]", "").toLowerCase();

						if (!inVocab(currentWordString)) {
							frequencyDictionary.add(new FrequencyWord(
									currentWordString, 1));

						} else {
							updateWord(currentWordString);
						}
					}
				}

				// create new output file
				file.createNewFile();
				FileWriter fw = new FileWriter(file.getAbsoluteFile());
				bw = new BufferedWriter(fw);

			} catch (IOException e) {
				// print io exception
				e.printStackTrace();
			} finally {

				// close bufferedReader
				try {
					if (br != null) {
						br.close();
					}

					if (bw != null) {
						bw.close();
					}
				} catch (IOException ex) {
					ex.printStackTrace();
				}

			}
		}
	}

	private void updateWord(String s) {

		for (FrequencyWord x : frequencyDictionary) {

			if (x.word.equals(s)) {
				x.frequency++;
			}
		}

	}

	private boolean inVocab(String s) {
		Boolean inVocabulary = false;

		for (FrequencyWord x : frequencyDictionary) {

			if (x.word.equals(s)) {
				inVocabulary = true;
			}
		}

		return inVocabulary;
	}
}
