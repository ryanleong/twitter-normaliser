//======================================================
// Knowledge Technologies Project 1 (2013)
// Twitter Normaliser
// 395463 (wsleong) Ryan Leong
//======================================================

package frequencyTechnique;

public class FrequencyWord {
	public String word;
	public int frequency;

	public FrequencyWord() {
		word = "";
		frequency = 0;
	}
	
	public FrequencyWord(String x, int y) {
		word = x;
		frequency = y;
	}
}
