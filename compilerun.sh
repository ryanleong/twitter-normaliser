#!/bin/bash

/usr/java1.6/bin/javac src/algorithms/Ngram.java src/algorithms/Soundex.java src/algorithms/NeedlemanWunsh.java src/abbrevationTechnique/Abbreviations.java src/frequencyTechnique/FrequencyWord.java src/frequencyTechnique/Frequency.java src/normaliser/WordToken.java src/normaliser/DictionaryWord.java src/normaliser/Main.java -d bin/ -classpath bin


#run
/usr/java1.6/bin/java -Xmx128m -cp bin:. normaliser.Main
exit
